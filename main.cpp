#include <iostream>
#include <string>
using namespace std;

int main() {
    string usrStr1;
    string usrStr2;
    int hDistance = 0;
    int i;
    bool taskIncomplete = true;
    bool validString = false;

    //A friendly greeting
    cout << "Hello, we will now find the Hamming Distance between two strings. These strings must be the same length, and have a length greater than zero.\n" << endl;

    //A while loop that will continue to run until taskIncomplete is set to false. This happens when the user inputs the sentinel value
    while (taskIncomplete) {
        hDistance = 0;
        validString = false;
        while (!validString){
            //asking for the first string
            cout << "Please enter the first string (enter -99 to exit): ";
            getline(cin, usrStr1);
            cout << endl;

             //checking if the user's string is the sentinel value, and breaking the loop if necessary
            if (usrStr1 == "-99") {
                cout << "This is goodbye :(";
                taskIncomplete = false;
                break;
            }
            //checking if the string is of length zero
            if (usrStr1.length() == 0) {
                cout << "Invalid String, the string cannot have length zero.\n";
                continue;
            }
            else {
                validString = true;
            }
        }
        //breaks out of the outer loop if taskIncomplete is false
        if (!taskIncomplete) {
            break;
        }

        //Resetting validString
        validString = false;
        while (!validString){
            //asking for the second string
            cout << "Please enter the second string (enter -99 to exit): ";
            getline(cin, usrStr2);
            cout << endl;

            //checking if the user's string is the sentinel value, and breaking the loop if necessary
            if (usrStr2 == "-99") {
                cout << "This is goodbye :(";
                taskIncomplete = false;
                break;
            }
            //checking if the string is of length zero
            if (usrStr2.length() == 0) {
                cout << "Invalid String, the string cannot have length zero.\n";
                continue;
            }
            else {
                validString = true;
            }
        }
        //breaks out of the outer loop if taskIncomplete is false
        if (!taskIncomplete) {
            break;
        }

        //checks that the strings are of equal length, and begins the loop anew if they are not of wqual length
        if (usrStr1.length() != usrStr2.length()) {
            cout << "These Strings are not the same length.\n\n";
            continue;
        }

        //calculates the Hamming Distance
        for (i = 0; i < usrStr1.length(); ++i) {
            if (usrStr1.at(i) != usrStr2.at(i)) {
                hDistance += 1;
            }
        }

        //outputs the results
        cout << "The Hamming Distance between \"" << usrStr1 << "\" and \"" << usrStr2 << "\" is " << hDistance << endl;
        cout << "Let's do this again :)\n\n";
        
    }

    return 0;
}